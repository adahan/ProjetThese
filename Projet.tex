\documentclass{article}
% Packages
\usepackage[francais]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{array}
\usepackage{tikz}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage[shortlabels]{enumitem} % 
\usepackage{fullpage} 
\usepackage{hyperref} %% Garder comme dernier package.
% Commandes
\newcommand{\NP}{\ensuremath{\textsf{NP}}}
\newcommand{\cNP}{\ensuremath{\textsf{co-NP}}}
\newcommand{\NPcNP}{\ensuremath{\textsf{NP} \cap \textsf{coNP}}}
\newcommand{\PT}{\ensuremath{\textsf{P}}}
\newcommand{\FP}{\ensuremath{\textsf{FP}}}
\newcommand{\FNP}{\ensuremath{\textsf{FNP}}}
\newcommand{\TFNP}{\ensuremath{\textsf{TFNP}}}
\newcommand{\QBF}{\ensuremath{\textrm{-QBF}}}
\newcommand{\argmin}{\ensuremath{\mathrm{argmin}}}
\newcommand{\dom}{\ensuremath{\mathrm{dom}}}
\newcommand{\osc}{\ensuremath{\mathrm{osc}}}
\newcommand{\diam}{\ensuremath{\mathrm{diam}}}
\newcommand{\Int}{\ensuremath{\mathrm{Int}}}
\newcommand{\Id}{\ensuremath{\mathrm{Id}}}
\newcommand{\Ker}{\mathrm{Ker}}
\newcommand{\card}{\mathrm{card}}
\renewcommand{\Im}{\mathrm{Im}\ }
\newcommand*{\EnQuR}[2]%
{\ensuremath{%
    #1/\!\raisebox{-.65ex}{\ensuremath{\mathcal{#2}}}}}
\newcommand*{\EnQuA}[2]
{\raisebox{-.65ex}{\ensuremath{%
    #1\backslash\!\raisebox{.65ex}{\ensuremath{#2}}}}}
 \newcommand{\tq}{\ensuremath{\ |\ }}
% Théorèmes
\newtheorem{thm}{Théorème}
\newtheorem{lem}{Lemme}
\newtheorem{corol}{Corollaire}
\newtheorem{prop}{Proposition}
\newtheorem{propr}{Propriété}
\newtheorem{proprs}{Propriétés}
\theoremstyle{definition}
\newtheorem{defi}{Définition}
\newtheorem{defis}{Définitions}
\newtheorem{exe}{Exemple}
\newtheorem{exes}{Exemples}
\newtheorem{exo}{Exercice}
\newtheorem{exi}{Exercices}
\theoremstyle{remark}
\newtheorem{rem}{Remarque}
\newtheorem{rems}{Remarques}
\title{Projet de thèse}
\author{Anatole Dahan}
\date\today
\begin{document}
\maketitle
\section*{Motivations}
La complexité computationnelle est l'étude des ressources nécessaires à l'accomplissement d'une tâche computationnelle. Ce questionnement est un sujet de recherche majeur depuis les années 60, en ce qu'il affecte plusieurs autres domaines de l'informatique, qu'elle soit appliquée ou fondamentale (à travers par exemples la cryptographie, les contraintes, la théorie des bases de données...) mais aussi à de nombreux domaines des sciences où les processus algorithmiques jouent un rôle clé comme, par exemple, en économie (\emph{blockchains}) ou en sciences politiques (théorie du vote, protocoles de communication, voire questions écologiques liées au coût énergétique du calcul).
Afin d'obtenir une meilleur compréhension des problèmes au cœur de la théorie de la complexité, de nombreuses pistes ont été empruntées, et l'une d'entre elles est la complexité descriptive. Son objectif est de caractériser les différentes classes de complexité par des paradigmes logiques. On dit qu'une logique $\mathcal L$ \emph{capture} une classe de complexité $\mathcal C$ si \begin{itemize}
\item Pour un énoncé de $\mathcal L$ fixé, la vérification que cet énoncé est vrai est un problème de $\mathcal C$\footnote{pour être parfaitement précis, la procédure consistant, étant donné un énoncé de $\mathcal L$, à fournir un témoin de l'appartenance du problème à $\mathcal C$ doit être récursive}
\item Pour un problème de $\mathcal C$ fixé, il existe un énoncé de $\mathcal L$ décrivant ce problème.
\end{itemize}
Par exemple, le résultat fondateur de la complexité descriptive, dû à Fagin\cite{Fagin}, est que la logique du second ordre existentiel capture $\NP$. De même, Immerman\cite{Immerman} et Vardi\cite{Vardi} ont montré que $\PT$ est capturé par la logique du premier ordre, muni d'un opérateur de points fixes, sur les structures totalement ordonnées. 

De tels résultats sont particulièrement intéressants. En effet, ils permettent de lier la quantité  de ressources nécessaire à la résolution d'un problème -- notion intimement dépendante du modèle de calcul -- à l'étendue du système logique nécessaire à sa définition, c'est à dire aux \emph{mécanismes de pensée} que sa définition requiert. Au delà de cette perspective philosophiquement passionnante, obtenir des logiques capturant les classes de complexité revêt un intérêt capital pour la complexité : la séparation de classes de complexité se réduit à la séparation de logiques, et permet donc d'appliquer les méthodes fournies par la théorie des modèles finis.
%La question de l'énumérabilité des classes de Turing est assez subtile. La première observation à faire est que $\PT$ et $\NP$ sont récursivement énumérables : étant donné une énumération des couples $(M_i,p_i)$, avec $M_i$ une machine de Turing (déterministe pour $\PT$, non déterministe pour $\NP$) et $p_i$ un polynôme, on obtient une énumération de la classe en considérant $\varphi(i,w)$ définie par \og\emph{$M_i$ accepte $w$ en un temps inférieur à $p_i(|w|)$}\fg. $\varphi$ est décidable, et pour tout problème $\mathcal P$ dans $\PT$ (resp. $\NP$), il existe un $i$ tel que $\mathcal P = \{w, \varphi(i,w)\}$. Si $\mathcal C$ est une classe de fonctions énumérable, et $\mathcal C'$ une classe de problèmes admettant un problème complet pour des $\mathcal C$-réductions, alors $\mathcal C'$ est énumérable.

Dans ce contexte, le fait que le résultat d'Immerman-Vardi cité plus haut porte sur les structures ordonnées est particulièrement important. En effet, parmi les méthodes principales permettant de prouver l'inexpressibilité d'une propriété $\mathcal P$ dans une logique, celles inspirées des jeux d'Ehrenfeucht-Fraïssé \cite{Ehrenfeucht}\cite{Fraisse} ne fonctionnent pas sur les structures ordonnées. Sans en donner une définition exacte, ces méthodes consistent en la définition d'une famille dénombrable  $(\equiv_n)$ de relations d'équivalences particulières sur les structures, toutes plus faibles que la relation d'isomorphisme, telles que si pour tout $n$, il existe deux structures $A_n$ et $B_n$ telles que $A_n\equiv_n B_n$, $A_n\models \mathcal P$ et $B_n\not\models\mathcal P$, alors la propriété $\mathcal P$ n'est pas définissable dans la logique considérée. Ce type de méthode ne fonctionne plus lorsqu'on fournit un ordre total sur les structures. En effet, si $A$ et $B$ sont deux structures non-isomorphes, $A\not\equiv_n B$ dès que $n\ge 2$. À l'inverse, si l'on retire l'ordre sur les structures, on peut prouver par ce type de technique que la logique du premier ordre avec opérateur de point fixe ne permet même pas d'exprimer le fait qu'une structure soit de cardinalité paire.

Ceci motive la recherche d'une logique pour $\PT$ qui ne nécessite pas d'ordre sur les structures. Une définition exacte en est donnée par Gurevich dans \cite{Gurevich}, où il conjecture aussi qu'une telle logique n'existe pas. On peut retracer cette conjecture jusqu'à \cite{Chandra}, aujourd'hui encore irrésolue.

Mais, si l'existence d'une logique satisfaisante pour $\PT$ est un problème ouvert, le théorème de Fagin énoncé précédemment donne une caractérisation élégante de $\NP$, sans ordre. En fait, cette caractérisation s'étend à toutes les classes de la hiérarchie polynomiale. On rappelle que la hiérarchie polynomiale est définie (voir \cite{Hierarchy}) ainsi : 
\begin{align*}
\Sigma^p_0 &=\PT\quad &\Delta^p_0 &=\PT \qquad &\Pi^p_0 &= \PT\\
\Sigma^p_{n+1} &= \NP^{\Sigma^p_n} &\Delta^{p}_{n+1}& = \PT^{\Sigma^{p}_n} &\Pi^p_{n+1} &= \cNP^{\Sigma^p_n}
\end{align*}
On obtient donc $\Sigma^p_1 = \NP$, $\Pi^p_1 = \cNP$, $\Sigma^p_2 =\NP^\NP$,  et le théorème de Fagin se généralise à tous les $\Sigma^p_n$ et $\Pi^p_n$ dans la hiérarchie : un problème est dans $\Sigma^p_n$ (resp. $\Pi^p_n$) ssi il est définissable par une formule du second ordre commençant par une quantification existentielle (resp. universelle), et contenant $n-1$ alternances de quantification.
\section*{Objectifs}

Ainsi, la hiérarchie semble très bien se comporter d'un point de vue logique ; bien mieux que $\PT$. Ceci motive une première question : les classes $\Delta^p_n$ admettent-elles une caractérisation logique ? Cette question est une généralisation de l'existence d'une logique pour $\PT$. Cependant, dans le cas où $n\ge 2$, des outils supplémentaires sont à notre disposition, en particulier, la canonisation de graphe\footnote{Un algorithme de canonisation de graphe est une procédure, qui à tout graphe associe un représentant canonique de sa classe d'isomorphisme}, qui permet de résoudre la question. En effet, si l'ensemble des machines appartenant à une classe de complexité est énumérable\footnote{et c'est le cas pour toutes les classes de complexité évoquées ici, excepté les $\Sigma^p_n\cap \Pi^p_n$}, le seul obstacle nous empêchant de considérer cette énumération comme une logique, au sens défini par Gurevich, est le suivant : si chaque machine est vue comme une formule, il faut qu'elle traite également les structures isomorphes. Dans le cas où l'on peut calculer une canonisation des structures au sein de notre classe de complexité, l'énumération des machines appartenant à la classe, chacune précomposée par la canonisation de graphe, forme alors bel et bien une logique au sens de Gurevich. Une telle logique est assez artificielle, et on peut alors se demander si les $\Delta^p_n$ admettent des logiques \emph{naturelles}, qui nous éclaireraient sur les limites de l'obtention d'une logique pour $\PT$.

On trouvera une étude détaillée des liens entre canonisation de graphes et capture logique dans \cite{GroheDC}. J'ai déjà été confronté à ces questions lors de mon stage de M1 avec Anuj Dawar. Au terme de ce stage, nous avons obtenu des résultats en cours de publication\cite{DahanDawar}, concernant la possibilité de l'existence d'une logique pour $\Sigma^p_n\cap \Pi^p_n$. En effet, contrairement aux hiérarchies arithmétique, borélienne\cite[resp. 3F.7 et 1D.4]{moschovakis} et projective\cite{Kechris}\footnote{sur des espaces topologiques raisonnables, comme les polonais}, la hiérarchie polynomiale ne vérifie pas $\Sigma_n\cap\Pi_n = \Delta_n$. 

Ainsi, il semble que, d'un point de vue logique, $\PT$ et les classes qu'elle engendre sont moins naturelles. Alors, pourrait-on refonder la hiérarchie polynomiale pour y obtenir une meilleure adéquation des $\Delta_n$ en son sein ? 
J'aimerais construire de nouvelles hiérarchies similaires à la hiérarchie polynomiale, c'est à dire basées sur la même méthode de construction : étant donnée une classe $\mathcal X$, on pose
\begin{align*}
\Sigma^\mathcal X_0 &=\mathcal X\quad &\Delta^\mathcal X_0 &=\mathcal X \qquad &\Pi^\mathcal X_0 &= \mathcal X\\
\Sigma^\mathcal X_{n+1} &= \NP^{\Sigma^\mathcal X_n} &\Delta^\mathcal X_{n+1}& = \mathcal X^{\Sigma^\mathcal X_n} &\Pi^\mathcal X_{n+1} &= \cNP^{\Sigma^\mathcal X_n}
\end{align*}
Le but d'une telle démarche est d'étudier des hiérarchies proches de la hiérarchie polynomiale classique, au sens où $\Sigma^\mathcal X_n = \Sigma^p_n$ et $\Pi^\mathcal X_n = \Pi^p_n$ à partir d'un certain $n$ (ceci implique $\mathcal X\subseteq \NP\cap\cNP$, ou des conditions similaires). De premiers exemples de $\mathcal X$ potentiels sont : 
\begin{enumerate}
\item $\PT^\mathrm{GI}$, où $\mathrm{GI}$ représente le problème d'isomorphisme de graphe.
\item $\NP\cap\cNP$. Cette classe n'est pas énumérable, mais qu'arrive-t-il aux $\Delta^\mathcal X_n$, $n\ge 2$ ? 
\item $\mathrm{FO + LFP + C}$ : c'est une extension de la logique de points fixes, permettant de compter. Définie dans \cite{FOLFPC}, c'est un premier pas vers une logique pour $\PT$. Cependant, Cai, Fürer et Immerman\cite{CFI} ont prouvé qu'elle ne capture pas exactement $\PT$.
\item Ce résultat de séparation motive la construction de Dawar et al.\cite{FOLFPRK} d'une logique plus puissante, bien que toujours évaluable dans $\PT$. Il s'agit de $\mathrm{FO + LFP + rk}$, où l'on ajoute à la logique de point fixe un opérateur capable de calculer le rang d'une matrice. On ne connait pas, aujourd'hui, de propriété dans $\PT$ qui ne soit pas définissable dans cette logique.
\end{enumerate} 
Les propriétés que nous serons amenés à chercher sont les suivantes : 
\begin{itemize}
\item A-t-on $\Sigma_n^\mathcal X\cap \Pi^\mathcal X_n$ énumérable ? égal à $\Delta^\mathcal X_n$ ?
\item Existe-t-il une logique naturelle capturant $\Delta^\mathcal X_n$ ?
\end{itemize}


Inversement, il serait intéressant d'obtenir des limites sur les résultats qu'on peut espérer obtenir par une telle démarche : 
\begin{itemize}
\item Est-ce que, s'il existe $n$ tel que $\Sigma^\mathcal X_n = \Sigma^p_n$, et $\Pi^\mathcal X_n = \Pi^p_n$, il existe $m$ tel que $\Delta^\mathcal X_m = \Delta^p_m$ ?
\item Avec l'hypothèse supplémentaire $\mathcal X$ énumérable ?
\end{itemize}
Finalement, une autre approche semble intéressante pour étudier ces questions. Dans la hiérarchie arithmétique, on définit $\Delta^0_n$ comme étant le plus petit ensemble de formules contenant $\Sigma^0_{n-1}$ et stable par opérations booléennes et \emph{quantification bornée}. Les formules $\Delta^0_0$ sont les formules dont toutes les quantifications sont bornées. Enfin, on vérifie aisément que les formules $\Sigma^0_n$ (ou $\Pi^0_n$) sont stables par quantification bornée. S'il est difficile de concevoir une quantification de second ordre bornée, il pourrait être intéressant d'identifier des opérations logiques, augmentant le pouvoir d'expression de la logique du premier ordre, qui stabilisent les classes $\Sigma^1_n$ et $\Pi^1_n$. Cette démarche pourrait fournir de nouveaux candidats de classes logiques sur lesquelles fonder une hiérarchie.

\section*{Encadrement}

Pour mener à bien un tel projet de thèse, je ne peux imaginer de meilleur directeur qu'Arnaud Durand, professeur à l'université Paris Diderot, où j'ai eu la chance de suivre ses cours de logiques durant ma licence, et son cours de complexité descriptive ce semestre. Nos longues discussions, et nos intérêts scientifiques communs m'assurent d'une entente propice à un futur travail ensemble. C'est par lui que j'ai découvert ce domaine passionnant et grâce à lui que j'ai rencontré Anuj Dawar avec qui j'ai travaillé durant mon stage de recherche de M1. Ses connaissances en complexité descriptive, et plus généralement à la limite entre complexité, logique et mathématiques me seront d'une grande aide dans ma perspective de recherche.

Néanmoins, certaines des problématiques sous-jacentes aux objectifs décrits plus hauts font appel à une connaissance pointue concernant le problème de l'isomorphisme de graphes. C'est pourquoi je serais honoré d'être co-dirigé par Luc Segoufin. En effet, il a beaucoup étudié ces questions, et il est spécialiste en théorie des bases de données, domaine intimement lié à la complexité descriptive. Il est aussi mon tuteur à l'ENS, et j'ai déjà entrepris un projet de recherche encadré avec lui, au sujet de l'isomorphisme de graphes, l'année dernière, qui fut une très bonne expérience.

Enfin, Anuj Dawar est aussi un spécialiste des questions mentionnées dans ce projet, et il sera sûrement un interlocuteur important au cours de l'évolution de mes recherches.
\bibliography{BigBib}
\bibliographystyle{plain}
\end{document}

au calcul d'une fonction calculable. On peut voir ce questionnement comme un raffinement de la calculabilité : si l'on a aujourd'hui une bonne compréhension de ce qu'une machine de Turing peut calculer, il y a encore beaucoup de zones d'ombres quant à la quantité de temps et d'espace nécessaire pour effectuer un tel calcul. Par exemple, on ne sait pas si la puissance d'une machine de Turing augmente si, lorsque son temps de calcul est borné polynomialement, on lui donne accès à un indice de taille polynomiale. C'est la question $\PT\stackrel ?= \NP$.
Si la classe $\PT$ des problèmes calculables en temps polynomiale est tant étudiée, c'est parce qu'elle représente (ou plutôt contient) l'ensemble des problèmes \emph{faisables}, c'est à dire qu'on peut espérer mener à bien sur un ordinateur. De plus, elle satisfait des propriétés naturelles intéressante : elle est close par composition, négation et récurrence bornée.

Afin de s'attaquer aux questions de complexité, plusieurs pistes ont été étudiées, l'une d'entre elle est la complexité descriptive. Il s'agit de prouver des conn
